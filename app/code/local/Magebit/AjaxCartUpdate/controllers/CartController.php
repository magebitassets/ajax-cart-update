<?php
require_once Mage::getModuleDir( 'controllers', 'Mage_Checkout' ) . DS . 'CartController.php';

/**
 * Magebit_AjaxCartUpdate
 *
 * @category     Magebit
 * @package      Magebit_AjaxCartUpdate
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2015 Magebit, Ltd. (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Magebit_AjaxCartUpdate_CartController extends Mage_Checkout_CartController
{
	/**
	 * Updates item qty in cart and returns the new blocks
	 *
	 * @return mixed
	 */
	public function ajaxMassUpdateAction()
	{
		$response = array();
		$this->loadLayout();

		$quote = $this->_getQuote();
		$items = $this->getRequest()->getParam( 'cart', array() );

		foreach ( $quote->getAllVisibleItems() as $item ) {
			if ( !isset( $items[ $item->getId() ] ) ) {
				continue;
			}

			$updateRequest = $items[ $item->getId() ];

			if ( !isset( $updateRequest[ 'qty' ] ) || $updateRequest == $item->getQty() ) {
				continue;
			}

			$item->setQty( $updateRequest[ 'qty' ] );
			$item->save();
		}

		// save cart
		$this->_getCart()->save();

		/*
		 * Add totals and items to the output
		 */
		/** @var Mage_Checkout_Block_Cart_Abstract $cartBlock */
		$cartBlock            = $this->getLayout()->getBlock( 'checkout.cart' );
		$response[ 'totals' ] = $cartBlock->getChild( 'totals' )->toHtml();
		$response[ 'items' ]  = '';

		foreach ( $cartBlock->getItems() as $item ) {
			$response[ 'items' ] .= $cartBlock->getItemHtml( $item );
		}

		/*
		 * Add minicart & quantity
		 */

		if (Mage::getStoreConfig('checkout/cart_link/use_qty')) {
			$qty = $this->_getCart()->getItemsQty();
		} else {
			$qty = $this->_getCart()->getItemsCount();
		}

		$response[ 'minicart' ] = $this->getLayout()->getBlock( 'minicart_content' )->toHtml();
		$response[ 'qty' ]      = $qty;

		// send json response
		$this->getResponse()->clearHeaders()->setHeader( 'Content-type', 'application/json', true );
		$this->getResponse()->setBody( json_encode( $response ) );
	}
}