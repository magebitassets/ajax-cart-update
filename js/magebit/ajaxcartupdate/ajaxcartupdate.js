/**
 * Magebit_AjaxCartUpdate
 *
 * @category     Magebit
 * @package      Magebit_AjaxCartUpdate
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2015 Magebit, Ltd.(http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
jQuery( function () {
	var cartUpdateTimeout = null;

	jQuery( document.body ).on( 'submit', '#shopping-cart-form', function ( e ) {
		e.preventDefault();
		return false;
	} );

	jQuery( document.body ).on( 'change', '#shopping-cart-form input.qty', function () {
		if ( cartUpdateTimeout ) {
			clearTimeout( cartUpdateTimeout );
		}

		cartUpdateTimeout = setTimeout( function () {
			var postData = jQuery( '#shopping-cart-form' ).serialize();
			jQuery.post(ajaxCartUpdateUrl, postData, function( response ) {
				if( response.totals !== undefined )
				{
					jQuery( '#shopping-cart-totals-table' ).replaceWith( response.totals );
				}

				if( response.items !== undefined )
				{
					jQuery( '#shopping-cart-table tbody' ).html( response.items );
				}

				if( response.minicart !== undefined )
				{
					jQuery( '#header-cart' ).html( response.minicart );
				}

				if( response.qty !== undefined )
				{
					jQuery( 'div.header-minicart span.count' ).text( response.qty );
				}

			}, "json");
		}, ajaxCartUpdateTimeout );
	} );
} );